﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveShoot : MonoBehaviour
{
  public Vector2 speed;
  private Vector2 movement;

  private Vector3 rightBottomCameraBorder;

  private Vector3 siz;

  // Start is called before the first frame update
  void Start()
  {
    rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
  }

  // Update is called once per frame
  void Update()
  {
    movement = new Vector2(speed.x, 0);
    GetComponent<Rigidbody2D>().velocity = movement;

    siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
    siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

    if(transform.position.x > rightBottomCameraBorder.x + (siz.x/2))
    {
      Destroy(gameObject);
    }
  }

  void OnTriggerEnter2D(Collider2D	collider)	{
    if (collider.gameObject.tag == "asteroid") {
      GameState.addScorePlayer(1);
    }

    //	Add	the	fade	script	to	the	gameObject	containing	this	script
    Destroy(collider.gameObject);

    //	Shoot	destroy
    Destroy(gameObject);
  }
}
