﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manager : MonoBehaviour
{

  private GameObject[] respawns;
  private Vector3 rightBottomCameraBorder;
  private Vector3 rightTopCameraBorder;
  private Vector3 siz;

  // Start is called before the first frame update
  void Start()
  {
    rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
    rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));
  }

  // Update is called once per frame
  void Update()
  {
    respawns = GameObject.FindGameObjectsWithTag("asteroid");
    if (respawns.Length < 10) {
      if (Random.Range(1, 100) == 50 || respawns.Length < 4) {
        siz.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x * gameObject.transform.localScale.x;
        siz.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y * gameObject.transform.localScale.y;
        Vector3 tmpPos = new Vector3(rightBottomCameraBorder.x + (siz.x/2), Random.Range(rightBottomCameraBorder.y + (siz.y/2), rightTopCameraBorder.y - (siz.y/2)), transform.position.z);
        GameObject gY	=	Instantiate(Resources.Load("asteroid"), tmpPos, Quaternion.identity) as GameObject;
      }
    }
  }
}
