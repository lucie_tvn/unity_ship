﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
  private static int scorePlayer;

  // Start is called before the first frame update
  void Start()
  {
    scorePlayer = 0;
  }

  void FixedUpdate()
  {
	   GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text	=	"" + scorePlayer;
	}

  public static void addScorePlayer(int toAdd)
  {
	   scorePlayer += toAdd;
	}

  public int getScorePlayer()
  {
	   return	scorePlayer;
	}
}
