﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAsteroid : MonoBehaviour
{
  public Vector2 speed;
  private Vector2 movement;

  private Vector3 leftBottomCameraBorder;
  private Vector3 rightBottomCameraBorder;
  private Vector3 rightTopCameraBorder;

  private Vector3 siz;

  // Start is called before the first frame update
  void Start()
  {
    leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
    rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
    rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));

    siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
    siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
    gameObject.transform.position = new Vector3(rightBottomCameraBorder.x+(siz.x/2), Random.Range(rightBottomCameraBorder.y, rightTopCameraBorder.y), transform.position.z);
  }

  // Update is called once per frame
  void Update()
  {
    movement = new Vector2(-speed.x, 0);
    GetComponent<Rigidbody2D>().velocity = movement;

    siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

    if(transform.position.x < leftBottomCameraBorder.x - (siz.x/2))
    {
      Destroy(gameObject);
    }
  }

  void OnTriggerEnter2D(Collider2D collider)
  {
    if (collider.name == "myShip") {
      if	(GameObject.FindGameObjectWithTag("life5")) GameObject.FindGameObjectWithTag("life5").AddComponent<fadeOut>();
      else if	(GameObject.FindGameObjectWithTag("life4")) GameObject.FindGameObjectWithTag("life4").AddComponent<fadeOut>();
      else if	(GameObject.FindGameObjectWithTag("life3")) GameObject.FindGameObjectWithTag("life3").AddComponent<fadeOut>();
      else if	(GameObject.FindGameObjectWithTag("life2")) GameObject.FindGameObjectWithTag("life2").AddComponent<fadeOut>();
      else if	(GameObject.FindGameObjectWithTag("life1")) GameObject.FindGameObjectWithTag("life1").AddComponent<fadeOut>();
    }
  }
}
