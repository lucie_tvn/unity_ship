using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posShip : MonoBehaviour
{
  // stockage des angles min et max de la caméra
  private Vector3 rightTopCameraBorder;
  private Vector3 leftTopCameraBorder;
  private Vector3 rightBottomCameraBorder;
  private Vector3 leftBottomCameraBorder;

  private Vector3 siz;

  void Start()
  {
    //calcul des angles avec conversion du monde de la camera au monde du pixel pour chaque coin
    leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
    rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
    leftTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,1,0));
    rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,1,0));

  }

  void Update()
  {
    /*
      Calcul de la taille du sprite auquel ce script est attaché
      Taille normal: gameObject.GetComponent<SpriteRenderer>().bounds.size
      On prend en compte les éventuelles transformations demandés lors de l'intégration du sprite dans l'échelle
      siz vaut alors la taille normale * par les déformations (notamment le zoom)
    */
    siz.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
    siz.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

    /*
      Positionnement du vaisseau contre le bord gauche de l'écran
    */
    // gameObject.transform.position = new Vector3(leftTopCameraBorder.x + (siz.x / 2), transform.position.x)

    /*
      Si la position en Y de notre vaisseau est inférieur à la limite basse de l'écran
      on repositionne le vaisseau en bas de l'écran
    */
    if(transform.position.y < leftBottomCameraBorder.y + (siz.y / 2))
    {
      gameObject.transform.position = new Vector3(transform.position.x, leftBottomCameraBorder.y + (siz.y / 2), transform.position.z);
    }

    /*
      Si la position en Y de notre vaisseau est supérieur à la limite haute de l'écran
      on repositionne le vaisseau en haut de l'écran
    */
    if(transform.position.y > leftTopCameraBorder.y - (siz.y / 2))
    {
      gameObject.transform.position = new Vector3(transform.position.x, leftTopCameraBorder.y - (siz.y / 2), transform.position.z);
    }

    /*
      Si la position en X du vaisseau est inférieure à la limite gauche de l'écran
      on le repositionne à gauche de l'écran
    */
    if(transform.position.x < leftBottomCameraBorder.x + (siz.x/2))
    {
      gameObject.transform.position = new Vector3(leftBottomCameraBorder.x+(siz.x/2), transform.position.y, transform.position.z);
    }

    /*
      Si la position en X du vaisseau est supérieure à la limite droite de l'écran
      on le repositionne à droite de l'écran
    */
    if(transform.position.x > rightBottomCameraBorder.x - (siz.x/2)){
      gameObject.transform.position = new Vector3(rightBottomCameraBorder.x-(siz.x/2), transform.position.y, transform.position.z);
    }
  }
}
